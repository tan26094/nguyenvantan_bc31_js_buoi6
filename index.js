/**
 * Bài 01: Tìm số nguyên dương nhỏ nhất sao cho tổng >10000
 */

var sum = 0;
document.getElementById("btn_tong1000").addEventListener("click", function () {
  for (var i = 0; sum <= 10000; i++) {
    sum += i;
    document.getElementById("result_01").innerHTML =
      "Số nguyên dương nhỏ nhât mà tổng lớn hơn 10000 là: " +
      i +
      " và tổng là: " +
      sum;
  }
});
/**
 * Bài 2: Viết chương trình nhập vào 2 số x, n tính tổng: S(n) = x + x^2
+ x^3 + … + x^n (Sử dụng vòng lặp và hàm)
 */
document.getElementById("btn_tong2").addEventListener("click", function () {
  var tong2 = 0;
  var xValue = document.getElementById("numx").value * 1;
  var nValue = document.getElementById("numn").value * 1;
  for (var i = 1; i <= nValue; i++) {
    tong2 += Math.pow(xValue, i);
  }
  document.getElementById("result_02").innerHTML = "Tổng là: " + tong2;
});
/**
 *Bài 03: Nhập vào n. Tính giai thừa 1*2*...n
 */
document.getElementById("btn_giaithua3").addEventListener("click", function () {
  var giaithua = 1;
  var nVal = document.getElementById("num3").value * 1;
  for (var i = 1; i <= nVal; i++) {
    giaithua *= i;
  }
  document.getElementById("result_03").innerHTML =
    "Giai thừ của n là: " + giaithua;
});
/**
 * Bài 04: Hãy viết chương trình khi click vào button sẽ in ra 10 thẻ div.
Nếu div nào vị trí chẵn thì background màu đỏ và lẻ thì
background màu xanh.
 */
document.getElementById("btn_inthe").addEventListener("click", function () {
  var iner = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      iner += `<div class="bg-primary text-white  p-2">Div chẳn</div>`;
    } else {
      iner += `<div class="bg-danger text-white  p-2">Div lẻ</div>`;
    }
  }
  document.getElementById("div_content").innerHTML = iner;
});

/**
 * Bài tập làm thêm: In số nguyên tốt
 */
function kiem_tra_snt(n) {
  var flag = true;

  if (n < 2) {
    flag = false;
  } else if (n == 2) {
    flag = true;
  } else if (n % 2 == 0) {
    flag = false;
  } else {
    // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
    for (var j = 3; j < Math.sqrt(n); j += 2) {
      if (n % j == 0) {
        flag = false;
        break;
      }
    }
  }
  return flag;
}

document.getElementById("btn-InSNT").addEventListener("click", function () {
  var numVal = document.getElementById("txt-num").value * 1;

  var DaySNT = "";
  for (var i = 1; i <= numVal; i++) {
    if (kiem_tra_snt(i)) {
      DaySNT += " " + i;
    }
  }
  document.getElementById("result").innerHTML = DaySNT;
});
